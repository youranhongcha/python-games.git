import os
import wx


class ControlPanel:
    def __init__(self, mine_panel):
        self.residue_mines       = 0
        self.time_consume_second = 0
        self.upper_panel_height  = 0
        self.down_panel_height   = 10
        self.smile_icon          = None
        self.icon_rect           = None
        self.possible_count_rect = None
        self.consume_time_rect   = None
        self.press_reset_icon    = False
        self.mine_panel          = mine_panel
        self.control_panel_txt_font   = None
        self.smile_icon = wx.Bitmap('res' + os.sep + 'smile.png')

    def occupy_height(self):
        return self.upper_panel_height + self.down_panel_height

    def set_upper_height(self, upper_height, client_rect):
        self.upper_panel_height = upper_height
        (icon_width, icon_height) = (self.upper_panel_height - 2, self.upper_panel_height - 2)
        self.icon_rect           = wx.Rect((client_rect.GetWidth()-icon_width) // 2, 1, icon_width, icon_height)
        rect_padding = 8
        rect_width = 50
        rect_height  = upper_height-2*rect_padding
        base_pnt_x = self.icon_rect.x + self.icon_rect.GetWidth()
        space = (client_rect.GetWidth() - base_pnt_x - rect_width * 2) // 6
        if (space < 0):
            space = 0
        
        self.possible_count_rect = wx.Rect(base_pnt_x + space, rect_padding, rect_width, rect_height)
        self.consume_time_rect   = wx.Rect(base_pnt_x + space * 2 + rect_width, rect_padding, rect_width, rect_height)
        
        padding_in_block = 4
        min_font_pixel_height = 10
        self.control_panel_txt_font = wx.Font(10, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
        font_pixel_height = rect_height - padding_in_block
        if (font_pixel_height < min_font_pixel_height):
            font_pixel_height = min_font_pixel_height
        self.control_panel_txt_font.SetPixelSize(wx.Size(0, font_pixel_height))

    def set_residue_mines_count(self, count, need_refresh):
        self.residue_mines = count
        if (need_refresh):
            if (self.possible_count_rect != None):
                self.mine_panel.RefreshRect(self.possible_count_rect)

    def set_consume_time(self, consume_time, need_refresh):
        self.time_consume_second = consume_time
        if (need_refresh):
            if (self.consume_time_rect != None):
                self.mine_panel.RefreshRect(self.consume_time_rect)

    def draw(self, dc, client_rect):
        blue_brush   = wx.Brush(wx.Colour(0, 255, 0))
        dc.SetBrush(blue_brush)
        icon_rect = self.icon_rect
        if (icon_rect == None):
            return
        if (self.smile_icon != None):
            (icon_x, icon_y) = (icon_rect.x, icon_rect.y)
            if (icon_rect.GetWidth() > self.smile_icon.GetWidth()):
                icon_x = icon_rect.x + ((icon_rect.GetWidth() - self.smile_icon.GetWidth()) // 2)
                icon_y = icon_rect.y + ((icon_rect.GetHeight() - self.smile_icon.GetHeight()) // 2)
                dc.DrawBitmap(self.smile_icon, icon_x, icon_y)
            else:
                dc.DrawBitmap(self.smile_icon, icon_rect.x, icon_rect.y)
        
        old_font = dc.GetFont()
        if (self.control_panel_txt_font != None):
            dc.SetFont(self.control_panel_txt_font)
        residue_rect_brush  = wx.Brush(wx.Colour(60, 128, 250))
        self._draw_str_in_rect(dc, str(self.residue_mines), self.possible_count_rect, residue_rect_brush)
        consume_second = self.time_consume_second
        if (consume_second > 999):
            consume_second = 999
        consume_rect_brush  = wx.Brush(wx.Colour(250, 128, 50))
        self._draw_str_in_rect(dc, str(consume_second), self.consume_time_rect, consume_rect_brush)
        dc.SetFont(old_font)

    def _draw_str_in_rect(self, dc, text, text_rect, rect_brush):
        if (text_rect != None):
            sz = dc.GetTextExtent(text)
            x = text_rect.x
            if (text_rect.GetWidth() > sz.GetWidth()):
                x = x + (text_rect.GetWidth() - sz.GetWidth()) // 2
            y = text_rect.y
            if (text_rect.GetHeight() > sz.GetHeight()):
                y = y + (text_rect.GetHeight() - sz.GetHeight()) // 2

            dc.SetBrush(rect_brush)
            dc.DrawRoundedRectangle(text_rect.x, text_rect.y, text_rect.GetWidth(), text_rect.GetHeight(), 4)
            dc.DrawText(text, x, y)
    
    def handle_mouse_left_down(self, event):
        (v_x, v_y) = event.GetPosition()
        if (self.icon_rect.Contains(v_x, v_y)):
            self.press_reset_icon = True
    
    def handle_mouse_left_up(self, event):
        (v_x, v_y) = event.GetPosition()
        if (self.icon_rect.Contains(v_x, v_y) and self.press_reset_icon):
            self.press_reset_icon = False
            if (self.mine_panel != None):
                self.mine_panel.reset_with_current_mode()
            return True
        else:
            self.press_reset_icon = False
            return False

