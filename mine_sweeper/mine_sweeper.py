import wx
from mine_panel import *


ID_3_3_GAME                 = 1001
ID_9_9_GAME                 = 1002
ID_16_16_GAME               = 1003
ID_16_30_GAME               = 1004


class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame(None, title="The Main Frame")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True


class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="", pos=wx.DefaultPosition, size=(800, 600),
                 style=wx.DEFAULT_FRAME_STYLE, name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title, pos, size, style, name)
        self._init_menu()
        self._init_status_bar()
        self._init_mine_panel()

    def _init_menu(self):
        menuBar = wx.MenuBar()
        
        filem = wx.Menu()
        menuBar.Append(filem, "&Game")
        mi = wx.MenuItem(filem, ID_3_3_GAME, "3 X 3 (3)")
        self.Bind(wx.EVT_MENU, self._on_choose_game, id=ID_3_3_GAME)
        filem.Append(mi)
        mi = wx.MenuItem(filem, ID_9_9_GAME, "9 X 9 (9)")
        self.Bind(wx.EVT_MENU, self._on_choose_game, id=ID_9_9_GAME)
        filem.Append(mi)
        mi = wx.MenuItem(filem, ID_16_16_GAME, "16 X 16 (40)")
        self.Bind(wx.EVT_MENU, self._on_choose_game, id=ID_16_16_GAME)
        filem.Append(mi)
        mi = wx.MenuItem(filem, ID_16_30_GAME, "16 X 30 (99)")
        self.Bind(wx.EVT_MENU, self._on_choose_game, id=ID_16_30_GAME)
        filem.Append(mi)

        helpm = wx.Menu()
        menuBar.Append(helpm, "&Help")
        self.SetMenuBar(menuBar)
        return True

    def _init_status_bar(self):
        self.statusBar = self.CreateStatusBar()

    def _init_mine_panel(self):
        rect = self.GetClientRect()
        self.mine_panel = MinePanel(self, sz=(rect.GetWidth(), rect.GetHeight()), st=wx.NO_BORDER)
        self.mine_panel.reset_blocks(16, 16, 40)

    def _on_choose_game(self, event):
        cmd = event.GetId()
        if (cmd == ID_3_3_GAME):
            self.mine_panel.reset_blocks(3, 3, 3)
        elif (cmd == ID_9_9_GAME):
            self.mine_panel.reset_blocks(9, 9, 9)
        elif (cmd == ID_16_16_GAME):
            self.mine_panel.reset_blocks(16, 16, 40)
        elif (cmd == ID_16_30_GAME):
            self.mine_panel.reset_blocks(16, 30, 99)
        self.mine_panel.Refresh()


if __name__=="__main__":
    app = MyApp(False)
    app.MainLoop()